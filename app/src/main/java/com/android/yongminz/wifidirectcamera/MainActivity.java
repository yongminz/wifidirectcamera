package com.android.yongminz.wifidirectcamera;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.provider.Settings;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity implements WifiP2pManager.PeerListListener {

    private WifiP2pManager manager;
    private final IntentFilter intentFilter = new IntentFilter();
    private WifiP2pManager.Channel channel;
    private BroadcastReceiver receiver = null;
    private List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Indicates a change in the Wi-Fi Peer-to-Peer status.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);

        // Indicates a change in the list of available peers.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);

        // Indicates the state of Wi-Fi P2P connectivity has changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);

        // Indicates this device's details have changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);

        findViewById(R.id.wifi_btn).setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                    }
                }
        );

        findViewById(R.id.lens_btn).setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        progressDialog = ProgressDialog.show(MainActivity.this, "Press back to cancel", "finding peers", true,
                                true, new DialogInterface.OnCancelListener() {

                                    @Override
                                    public void onCancel(DialogInterface dialog) {

                                    }
                                });
                        manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
                            @Override
                            public void onSuccess() {
                                Toast.makeText(MainActivity.this, "Discovery Initiated",
                                        Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(int reasonCode) {
                                Toast.makeText(MainActivity.this, "Discovery Failed : " + reasonCode,
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
        );

        findViewById(R.id.shutter_btn).setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        progressDialog = ProgressDialog.show(MainActivity.this, "Press back to cancel", "finding peers", true,
                                true, new DialogInterface.OnCancelListener() {

                                    @Override
                                    public void onCancel(DialogInterface dialog) {

                                    }
                                });
                        manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

                            @Override
                            public void onSuccess() {
                                Toast.makeText(MainActivity.this, "Discovery Initiated",
                                        Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(int reasonCode) {
                                Toast.makeText(MainActivity.this, "Discovery Failed : " + reasonCode,
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
        );

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
    }

    @Override
    public void onBackPressed() {
        manager.stopPeerDiscovery(channel,new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d("yongminz", "stopPeerDiscovery success!!!");
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.d("yongminz", "stopPeerDiscovery failed!!!");
            }
        });
        manager.cancelConnect(channel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d("yongminz", "cancelConnect success!!!");
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.d("yongminz", "cancelConnect failed!!!");
            }
        });
        super.onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    public void btnController(int btnID, boolean enable) {
        switch (btnID) {
            case R.id.wifi_btn :
                if(enable)
                    findViewById(R.id.wifi_btn).setBackgroundResource(R.drawable.enable_wifi_direct_btn_img);
                else
                    findViewById(R.id.wifi_btn).setBackgroundResource(R.drawable.desable_wifi_direct_btn_img);
        }

    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peerList) {
        peers.clear();
        peers.addAll(peerList.getDeviceList());
        for(int i = 0; i < peers.size(); i++) {
            WifiP2pDevice device = peers.get(i);
            Log.d("yongminz", "onPeersAvailable device name : " + device.deviceName + " addr : " + device.deviceAddress);
        }
        if (peers.size() == 0) {
            return;
        }
        /*WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.wps.setup = WpsInfo.PBC;
        manager.connect(channel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                // WiFiDirectBroadcastReceiver will notify us.
                Log.d("yongminz", "connect success!!!");
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(MainActivity.this, "Connect failed. Retry.",
                        Toast.LENGTH_SHORT).show();
            }
        });*/
    }

}