package com.android.yongminz.wifidirectcamera;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by yongminz on 2015-03-24.
 */
public class DeviceListView extends LinearLayout {

    private TextView mDevicename;

    public DeviceListView(Context context, WifiP2pDevice device) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.devicelist, this, true);
        mDevicename = (TextView) findViewById(R.id.devicename);
    }

    public void setText(String data) {
        mDevicename.setText(data);
    }
}
